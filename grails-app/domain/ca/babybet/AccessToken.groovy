package ca.babybet

class AccessToken {

    static final String PAYPAL_NAME = "PayPal"

    String name
    String token
    long expires = 0

    static constraints = {
        name unique: true
        token nullable: true
        expires nullable: true
    }
}
