package ca.babybet

class Bet {

    String json
    String email
    String weight
    String gender
    String paymentId
    String payerId
    String saleId
    String executeUrl
    String approvalUrl
    Boolean paid = false
    Date dateCreated


    static mapping = {
        json type: 'text'
    }

    static constraints = {
        json nullable: true
        gender inList: ["BOY", "GIRL"]
        email email: true
        paid nullable: true
        saleId nullable: true
        email nullable: true
        paymentId nullable: true
        payerId nullable: true
        executeUrl nullable: true
        approvalUrl nullable: true
        dateCreated nullable: true
    }

    /**
     * Return a string describing the bet.
     * @return a string describing the bet.
     */
    String getDescription() {
        return "I bet the baby is a $gender and weighs $weight."
    }
}
