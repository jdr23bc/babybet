package ca.babybet

class BetController {

    def payPalService

    def bet() {
        log.debug("Rendering bet view")
    }

    def placeBet(Bet bet) {
        log.debug("Placing bet")
        // Validate bet
        if (bet.hasErrors()) {
            // Log the errors and tell the user something is wrong
            bet.errors.each { error ->
                log.debug error
            }
            redirect(action: "technicalDifficulties")
        } else {
            // Build bet payment request to paypal
            if(payPalService.createPayment(bet)) {
                // Send the user to the approval url
                log.debug("Payment built - redirecting for approval")
                redirect(url: bet.approvalUrl)
            } else {
                log.debug("Failed to build payment on bet $bet")
                redirect(action: "technicalDifficulties")
            }
        }

    }

    def cancelBet() {
        redirect(action: "bet")
    }

    def approveBet() {
        log.debug("Payment approved. Executing payment ${params?.PayerID}.")

        def betToExecute = Bet.findByApprovalUrlLike( '%' + params.token + '%')

        if (betToExecute == null) {
            // The bet was not persisted! Big whoopsy
            redirect(action: "technicalDifficulties")
        } else if (betToExecute.paid) {
            // If the user is refreshing the payment screen render bet approved!
            render(view: "/bet/betApproved", model: [bet: betToExecute])
        } else {
            betToExecute.payerId = params.PayerID
            if (payPalService.executePayment(betToExecute)) {
                render(view: "/bet/betApproved", model: [bet: betToExecute])
            } else {
                redirect(action: "technicalDifficulties")
            }
        }
    }

    // Rendered if the bet was not approved
    def notApproved() {
        render(text: "Sorry. Unexpected response from PayPal - your payment was canceled.")
    }

    def technicalDifficulties() {
        render(text: "We're having some technical difficulties - please try again later!")
    }

}
