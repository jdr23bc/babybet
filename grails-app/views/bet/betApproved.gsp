<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Baby bet</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bet.css')}" type="text/css">
</head>
    <body>


    <center>
        <h1>Bet approved! Good luck!</h1>
        <img id="thumbsup" src="${resource(dir: 'images', file: 'thumbsup.png')}">
    </center>

    <script>
        var size = Math.min(Math.floor(window.innerHeight / 1.5), Math.floor(window.innerWidth / 1.5));
        thumbsup.height = size;
        thumbsup.width = size * 0.75;
    </script>

    <g:if test="${bet?.gender == 'BOY'}">
        <script>
            $("#overlay").hide();
            document.body.style.backgroundColor = "#25A6E1";
        </script>
    </g:if>
    <g:else>
        <script>
            $("#overlay").hide();
            document.body.style.backgroundColor = "#FF5DB1";
        </script>
    </g:else>

    </body>
</html>
