<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Baby bet</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-mobile.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bet.css')}" type="text/css">
</head>
<body data-role="none">
    <div id="pageContainer">
        <div id="appDescription">
            <b>Support Hannah and Koko!</b> Bet $5.30 and if you have the <b>closest
            guess of the sex and weight</b> you win! $5 goes into the pool and 30¢ goes to PalPal fees.
            The winner <b>splits the pool with baby Relleve</b>. Use the buttons and slider to make a bet!
        </div>
        <div id="controls">
            <button id="boyButton" data-role="none">Boy</button>
            <button id="girlButton" data-role="none">Girl</button>
            <!-- Initial guess is 1220z (7lb), with minimum of 0 and max of 240 oz (15lb) -->
            <label for="slider" class="ui-hidden-accessible">weight guess</label>
            <input name="slider" id="slider" min="0" max="240" value="122" type="range" data-highlight="true">
        </div>

        <div id="babyImage">
            <table height="100%" width="100%">
                <tbody>
                <tr>
                    <td align="center" valign="middle">
                        <image id="baby" src="${resource(dir: 'images', file: 'baby.png')}"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="betDescription">

            Use the buttons and slider above to make a bet.

        </div>
        <div id="bet">
            <g:form name="betForm" action="placeBet" data-ajax="false">
                <input type="hidden" name="gender" id="gender"/>
                <input type="hidden" name="weight" id="weight">
                <g:submitButton data-role="none" disabled="disabled" name="betButton" value="Bet!"/>
            </g:form>
        </div>
    </div>

    <script src="${resource(dir: 'js', file: 'spin.js')}"></script>
    <script src="${resource(dir: 'js', file: 'babybet.js')}"></script>
    <script src="${resource(dir: 'js', file: 'jquery-mobile.js')}"/>
</body>
</html>
