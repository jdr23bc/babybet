import ca.babybet.AccessToken

class BootStrap {

    def init = { servletContext ->
        new AccessToken(name: AccessToken.PAYPAL_NAME).save()
    }

    def destroy = {

    }
}
