package ca.babybet

import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.*

import static groovyx.net.http.ContentType.*

class PayPalService {

    def grailsApplication

    /**
     * Authenticates to PayPal and returns an OAuth2 access token.
     * @return access token string that may be used to make requests to the PayPal API, or null if the request failed
     */
    String authenticate() {
        AccessToken payPalToken = AccessToken.findByName(AccessToken.PAYPAL_NAME)
        if (payPalToken != null && payPalToken.token != null && payPalToken.expires > System.currentTimeMillis()) {
            log.debug("token still valid")
            return payPalToken.token
        }

        log.debug("Authenticating to PayPay.")
        def http = new HTTPBuilder(grailsApplication.config.paypal.url)

        http.request(POST, JSON) { req ->
            uri.path = '/v1/oauth2/token'
            headers.Authorization = "Basic $grailsApplication.config.paypal.authString"
            headers.Accept = JSON
            send URLENC, [grant_type: "client_credentials"]

            response.success = { resp, json ->
                log.debug "Success authenticating to paypal, expires in $json.expires_in."
                payPalToken.expires = System.currentTimeMillis() + json.expires_in.toLong() * 1000
                payPalToken.token = json.access_token
                payPalToken.save(flush: true)
                return payPalToken.token
            }

            response.failure = { resp ->
                log.debug "Failure authenticating to PayPal $resp.status."
                return null
            }
        }
    }

    /**
     * Build a paypal payment object with appropriate redirect URLs and a description of the bet. Send the transaction
     * object to Paypal. If the request is successful save payment details to the bet and return the approval URL. If the
     * server can't authenticate to PayPal return the 'technicalDifficulties' url
     *
     * @param bet the bet being made
     * @return true if the approval URL was retrieved, false otherwise
     */
    boolean createPayment(Bet bet) {
        String accessToken = authenticate()
        if (accessToken == null) {
            return grailsApplication.config.urls.technicalDifficulties
        }
        def http = new HTTPBuilder(grailsApplication.config.paypal.url)
        log.debug(grailsApplication.config.urls.betApproved)
        log.debug(grailsApplication.config.urls.betCanceled)
        def payment = [
                intent:"sale",
                redirect_urls: [
                        return_url:grailsApplication.config.urls.betApproved,
                        cancel_url:grailsApplication.config.urls.betCanceled
                ],
                payer: [
                        payment_method:"paypal"
                ],
                transactions: [
                        [
                            amount: [
                                total: "5.30",
                                currency: "CAD"
                            ],
                            description: bet.getDescription()
                        ]
                ]
        ]

        http.request(POST, JSON) { req ->
            uri.path = '/v1/payments/payment'
            headers.Authorization = "Bearer $accessToken"
            body = payment

            response.success = { resp, json ->
                log.debug "Successfully created payment. $json"
                if (json.state == "created" && json.intent == "sale") {
                    bet.paymentId = json.id
                    bet.executeUrl = json.links.find{ link -> link.rel == "execute" }.href
                    bet.approvalUrl = json.links.find{ link -> link.rel == "approval_url" }.href
                    bet.save(failOnError: true, flush: true)
                    return true
                } else {
                    return false
                }

            }

            response.failure = { resp, json ->
                log.debug "Failure creating paypal payment $resp.status."
                log.debug "$json"
                return false
            }

            response.failure = { resp ->
                log.debug "Failure creating paypal payment $resp.status."
                return false
            }
        }
    }

    /**
     * Execute a payment on a bet.
     *
     * @param bet the bet being made
     * @return true if the payment worked, false otherwise.
     */
    boolean executePayment(Bet bet) {
        log.debug("Executing Paypal payment.")
        String accessToken = authenticate()
        if (accessToken == null) {
            return false
        }

        log.debug(bet.executeUrl)
        def http = new HTTPBuilder(bet.executeUrl)
        http.request(POST, JSON) { req ->
            headers.Authorization = "Bearer $accessToken"
            body = [payer_id: bet.payerId]

            response.success = { resp, json ->
                log.debug "Payment execution is $resp.status. Json: $json"
                // TODO what other states should be looked at?
                if (json.state == "approved" || json.state == "pending" ) {
                    bet.email = json.payer.payer_info.email
                    bet.payerId = json.payer.payer_info.payer_id
                    bet.saleId = json.transactions.related_resources.sale.id
                    bet.json = json
                    bet.paid = true
                    bet.save(flush:true)
                    log.debug("Successful sale. $json")
                    return true
                } else {
                    log.debug "Failure executing paypal payment. status: $resp.status."
                    bet.json = json
                    bet.paid = null
                    bet.save(flush:true)
                    return false
                }
            }

            response.failure = { resp, json ->
                log.debug "Failure executing paypal payment. status: $resp.status."
                log.debug "$json"
                return false
            }

            response.failure = { resp ->
                log.debug "Failure executing paypal payment. status: $resp.status."
                return false
            }
        }
    }
}
