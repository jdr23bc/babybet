function getHumor() {
    var ret = ""
    var ounces = $("#slider").val()
    if (ounces == 0) {
        ret = ". Look, I know it looks like Hannah hasn't gained weight, but the kid is going to weigh <i>something</i>."
    }

    // Starts a 10lb
    if (ounces >= 160) {
        ret += "..."
    }
    // 11
    if (ounces >= 176) {
        ret += " uh..."
    }
    // 12
    if (ounces >= 192) {
        ret += " wow."
    }
    // 13
    if (ounces >= 208) {
        ret += " I sure hope not."
    }
    // 14
    if (ounces >= 224) {
        ret += " REALLY?"
    }
    // 15
    if (ounces >= 240) {
        ret = ".<br>If you win with this bet Hannah gets <b>all</b> of the winnings."
    }
    return ret
}

// Function called to build bet description
function updateBetDescription () {
    console.log("update")
    // Only update if a sex has been selected
    if($("#gender").val() == "BOY" || $("#gender").val() == "GIRL") {

        var description = "You're betting on a <b>" + $("#weight").val() + "</b> healthy baby <b>" + $("#gender").val() + "</b>";
        var funny = getHumor();

        $("#betDescription").html(description + funny);
        $("#betButton").removeAttr("disabled");
    } else {
        $('body').css('background', '#C0C0C0');
    }
    if ($("#gender").val() == "BOY") {
        $("body").css("background", "#25A6E1");
    }
    if ($("#gender").val() == "GIRL") {
        $("body").css("background", "#FF5DB1");
    }
}

// Function called to have various page elements reflect the value of the slider
function pageReflectSlider() {
    var guess = $("#slider").val()
    var pounds = Math.floor(guess / 16);
    var ounces = guess % 16;
    var weightGuess;
    if(ounces > 0) {
        weightGuess = pounds + "lb " + ounces + "oz";
    } else {
        weightGuess = pounds + "lb"
    }

    $("#weight").val(weightGuess);
    var babySize = .89*(guess);
    $("#baby").animate({ height: babySize + "px" }, 90);
    updateBetDescription();
}

// On click of the boy bet button turn the background blue & change the bet description
$("#boyButton").click(function(){
    $("#gender").val("BOY");
    updateBetDescription();
});

// On click of the girl bet button turn the background pink & change the bet description
$("#girlButton").click(function(){
    $("#gender").val("GIRL");
    updateBetDescription();
});

// When the user clicks the bet button then show the spinner & overlay
$("#betButton").click(function(){
    $("#overlay").show()
});

$(document).bind("pagecreate", function(event, ui) {
    // Set the spinner and hide the overlay
    var opts = {
            lines: 13, // The number of lines to draw
            length: 20, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#fff', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spin', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent in px
            left: '50%' // Left position relative to parent in px
    };
    var target = document.getElementById('overlay');
    var spinner = new Spinner(opts).spin(target);
    $("#overlay").hide()

    // Set listeners for the slider
    var lastCall = new Date()
    $("#slider").on("change", function() {
        var now = new Date();
        //only allow calls every tenth of a second
        if (Math.abs(now - lastCall) > 100) {
            lastCall = now;
            pageReflectSlider();
        }
    });

    $("#slider").on("slidestop", function() {
        pageReflectSlider();
    });
});

// Hide the overlay when the user leaves
$(window).bind("beforeunload", function() {
    //$("#overlay").hide()
});

pageReflectSlider();