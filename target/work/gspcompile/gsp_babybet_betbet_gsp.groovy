import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_babybet_betbet_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bet/bet.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',5,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',5,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir: 'css', file: 'jquery-mobile.css'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'css', file: 'bet.css'))
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(6)
createTagBody(1, {->
printHtmlPart(7)
expressionOut.print(resource(dir: 'images', file: 'baby.png'))
printHtmlPart(8)
createTagBody(2, {->
printHtmlPart(9)
invokeTag('submitButton','g',44,['data-role':("none"),'disabled':("disabled"),'name':("betButton"),'value':("Bet!")],-1)
printHtmlPart(10)
})
invokeTag('form','g',45,['name':("betForm"),'action':("placeBet"),'data-ajax':("false")],2)
printHtmlPart(11)
expressionOut.print(resource(dir: 'js', file: 'spin.js'))
printHtmlPart(12)
expressionOut.print(resource(dir: 'js', file: 'jquery-mobile.js'))
printHtmlPart(13)
})
invokeTag('captureBody','sitemesh',189,['data-role':("none")],1)
printHtmlPart(14)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1397934122000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
